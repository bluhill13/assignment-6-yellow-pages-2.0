﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yellow_pages_2._0
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public Person(string firstName, string lastName, int phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }
        /// <summary>
        /// Checks if there is a match for the provided searchWord
        /// </summary>
        /// <param name="searchWord"></param>
        /// <returns></returns>
        public bool CheckForMatch(string searchWord)
        {
            if (FirstName.Contains(searchWord, StringComparison.CurrentCultureIgnoreCase) ||
                    LastName.Contains(searchWord, StringComparison.CurrentCultureIgnoreCase)) return true;
            else if (PhoneNumber.ToString().Contains(searchWord)) return true;
            else return false;
        }
        /// <summary>
        /// Prints all info for this person
        /// </summary>
        public void PrintInfo()
        {
            if (PhoneNumber == 0) Console.WriteLine($"Name: {FirstName} {LastName}\nPhone number: not provided\n");
            else Console.WriteLine($"Name: {FirstName} {LastName}\nPhone number: {PhoneNumber}\n");
        }
    }
}
