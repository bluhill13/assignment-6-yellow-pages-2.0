﻿using System;
using System.Collections.Generic;

namespace Yellow_pages_2._0
{
    class Program
    {

        public static List<Person> ContactObjects = new List<Person>();
        public static bool title = true;
        static void Main(string[] args)
        {
            ContactObjects.Add(new Person("Thomas", "Blaalid", 97784171));
            ContactObjects.Add(new Person("Erlend", "Normann", 99999999));
            ContactObjects.Add(new Person("Sebastian", "Vettel", 11111111));
            ContactObjects.Add(new Person("Max", "Verstappen"));
            ContactObjects.Add(new Person("Lewis", "Hamilton"));
            string input = null;
            do
            {
                input = GetSearchWord();
                if (input != "exit") PerformSearch(input);
                Console.WriteLine("\n \tWrite exit to close program");
            } while (input != "exit");

        }
        
        public static string GetSearchWord()
        {
            if (title) Console.WriteLine("\n\t\tYellow pages search engine\n"); title = false;

            Console.Write("\n\tSearch: ");
            string input = Console.ReadLine();
            Console.Clear();
            return input;
        }
        /// <summary>
        /// Performs a search of all Person objects in list ContactObjects
        /// </summary>
        /// <param name="searchWord"></param>
        public static void PerformSearch(string searchWord)
        {
            int numberOfResults = 0;
            if (!title) Console.WriteLine("\n\t\tYellow pages search engine\n");
            Console.WriteLine("\n\tResults\n");
            foreach (Person item in ContactObjects)
            {
                if (item.CheckForMatch(searchWord))
                {
                    item.PrintInfo();
                    numberOfResults += 1;
                }
            }
            if (searchWord == "")
            {
                Console.WriteLine("\tYou searched for nothing, and therefore got everyone!\n");
            }
            else
            {
                Console.WriteLine($"\tSearch of '{searchWord}' gave {numberOfResults} results\n");
            }
        }
    }
}
